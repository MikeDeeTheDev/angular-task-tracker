import { Task } from "./interfaces/Task.interface";

export const TASKS: Task[] = [
  {
    id: 1,
    text: "Doctors Appointment",
    day: "May 5th at 2:30pm",
    reminder: true,
  },
  {
    id: 2,
    text: "Walk Dog",
    day: "Every Day at 12pm",
    reminder: true,
  },
  {
    id: 3,
    text: "Wash Car",
    day: "August 21st at 4:30pm",
    reminder: false,
  },
  {
    id: 4,
    text: "Tell Tasia to Get High",
    day: "Every Day at 4:20pm",
    reminder: true,
  },
];
