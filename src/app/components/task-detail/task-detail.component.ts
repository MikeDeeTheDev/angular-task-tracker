import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { Task } from "../../interfaces/Task.interface";
import { TaskService } from "src/app/services/task.service";

@Component({
  selector: "app-task-detail",
  templateUrl: "./task-detail.component.html",
  styleUrls: ["./task-detail.component.css"],
})
export class TaskDetailComponent implements OnInit {
  @Output() onUpdateTask: EventEmitter<Task> = new EventEmitter();

  text: string = "";
  day: string = "";
  reminder: boolean = false;
  id?: number;

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private location: Location
  ) {}

  ngOnInit(): void {
    const taskId = Number(this.route.snapshot.paramMap.get("id"));
    console.log("taskId: ", taskId);
    this.taskService.getTask(taskId).subscribe((task) => {
      this.text = task.text;
      this.day = task.day;
      this.reminder = task.reminder;
      this.id = task.id;
    });
  }

  onSubmit(): void {
    const updatedTask: Task = {
      id: this.id,
      text: this.text,
      day: this.day,
      reminder: this.reminder,
    };

    console.log("updatedTask: ", updatedTask);
    this.taskService.updateTask(updatedTask).subscribe(() => {
      this.location.back();
    });
  }

  goBack(): void {
    this.location.back();
  }
}
