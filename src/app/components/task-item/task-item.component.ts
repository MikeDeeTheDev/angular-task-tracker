import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Task } from "src/app/interfaces/Task.interface";
import { faTimes, faEdit } from "@fortawesome/free-solid-svg-icons";
import { Router } from "@angular/router";

@Component({
  selector: "app-task-item",
  templateUrl: "./task-item.component.html",
  styleUrls: ["./task-item.component.css"],
})
export class TaskItemComponent implements OnInit {
  @Input() task?: Task;
  @Output() onDeleteTask: EventEmitter<Task> = new EventEmitter();
  @Output() onToggleReminder: EventEmitter<Task> = new EventEmitter();
  faTimes = faTimes;
  faEdit = faEdit;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  onDelete(task?: Task) {
    this.onDeleteTask.emit(task);
  }

  onToggle(task?: Task) {
    this.onToggleReminder.emit(task);
  }

  onEdit(task?: Task) {
    console.log("task: ", task);
    this.router.navigate([`/task/${task?.id}`]);
  }
}
