import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AboutComponent } from "./components/about/about.component";
import { TaskDetailComponent } from "./components/task-detail/task-detail.component";

import { TasksComponent } from "./components/tasks/tasks.component";

const appRoutes: Routes = [
  {
    path: "",
    component: TasksComponent,
  },
  {
    path: "task/:id",
    component: TaskDetailComponent,
  },
  {
    path: "about",
    component: AboutComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(appRoutes, { enableTracing: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
